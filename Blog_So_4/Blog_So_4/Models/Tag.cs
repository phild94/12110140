﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_So_4.Models
{
    public class Tag
    {
        [Required]
        public int TagID { get; set; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự từ 10 đến 100", MinimumLength = 10)]
        public string Content { get; set; }


        public virtual ICollection<Post> Posts { get; set; }
    }
}