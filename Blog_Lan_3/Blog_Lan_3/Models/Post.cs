﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_Lan_3.Models
{
    public class Post
    {
       
        public int ID { set; get; }

        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(500, ErrorMessage = "Số lượng kí tự nằm trong khoảng từ 20 đến 500", MinimumLength = 20)]
        public string Title { set; get; }


        [Required(ErrorMessage = "Không được bỏ trống")]
        [StringLength(111111111, ErrorMessage = "Số lượng ký tự tối thiểu là 50 ký tự", MinimumLength = 50)]
        public string Body { set; get; }


        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ!")]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Nhập ngày hợp lệ")]
        [DataType(DataType.DateTime, ErrorMessage = "Nhập ngày hợp lệ!")]
        public DateTime DateUpdated { get; set; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}