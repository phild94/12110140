﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_So_4.Models;

namespace Blog_So_4.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, String Content)
        {
            if (ModelState.IsValid)
            {

                //post.DateCreated = DateTime.Now;
                //post.DateUpdated = DateTime.Now;
                //Tag tag1 = new Tag();
                //tag1.Content = Content;
                //if (post.Tags == null)
                //    post.Tags = new List<Tag>();
                //post.Tags.Add(tag1);
                //if (tag1.Posts == null)
                //    tag1.Posts = new List<Post>();
                //tag1.Posts.Add(post);
                //db.Posts.Add(post);
                //db.Tags.Add(tag1);

                //int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                //   Where(y => y.UserName == User.Identity.Name).Single().UserId;

                //post.UserProfileUserId = userid;
                //db.SaveChanges();
                //return RedirectToAction("Index");


                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                   Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;

                //Tạo List các Tag

                List<Tag> Tags = new List<Tag>();
                //Tách các tag theo dấu phẩy
                string[] TagContent = Content.Split(',');

                //Lập các tag vừa tách
                foreach (string item in TagContent)
                {
                    //Tìm xem TagContent đã có chưa
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        // Nếu có tag rồi thì add thêm post vào
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        tagExits = new Tag();
                        tagExits.Content = item;
                        if (tagExits.Posts == null)
                            tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //Add vào list các Tag 
                    Tags.Add(tagExits);
                }

                //Gán List Tag cho Post

                post.Tags = Tags;
                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult ViewTaginPost(int id=0)
        {
            Tag tag = db.Tags.Find(id);
            return View(tag);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}