// <auto-generated />
namespace Blog_So_4.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class Lan5 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Lan5));
        
        string IMigrationMetadata.Id
        {
            get { return "201411180530486_Lan5"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
