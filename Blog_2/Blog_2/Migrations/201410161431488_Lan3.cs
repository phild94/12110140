namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Lan3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Posts", "AccountID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Posts", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "AccountID" });
            DropForeignKey("dbo.Posts", "AccountID", "dbo.Accounts");
            DropColumn("dbo.Posts", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
