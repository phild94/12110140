﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Blog_Lan_3.Models
{
    public class BlogDbContext : DbContext
    {
        public DbSet<Post> Posts { set; get; } // Co s vi day la tap hop cac doi tuong => so nhieu
        public DbSet<Comment> Comments { set; get; }
       


        //Ket theo quan he nhieu nhieu
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Post>().HasMany(d => d.Tags).WithMany(p => p.Posts)
                .Map(t => t.MapLeftKey("PostID")
                .MapRightKey("TagID").ToTable("Tag_Post"));
        }

        public DbSet<Tag> Tags { set; get; }
    }
}